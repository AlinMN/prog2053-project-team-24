var mysql = require('mysql');
var express = require('express');
var app = express();
var PORT = 8080;


app.get('/', function(request, response){
    fetchData(response);
    console.log('Done!');
});

db.connect(function(error){
    if (error)
        throw error;
    console.log("Connected to DATABASE");
})

function executeQuery(sql, cb){     // cb = callback function
    db.query(sql, function(error, result, field){
        if(error) 
            throw error;
        cb(result);
    })
}


function fetchData(response) {
    executeQuery("Select * from posts", function(result) {
        console.log(result);
        response.write('<table><tr>');
        for(var column in result[0]) {
            response.write('<td><label>' + column + '</label></td>');
            response.write('</tr>');
        }
        for(var row in result) {
            response.write('<tr>');
            for (var column in result[row]) {
                response.write('<td><label>' + result[row][column] + '</label></td>');
            }
            response.write('</tr>');
        }
        response.end('</table>');
    });
}


app.listen(PORT, function() {
    console.log('Listening to Port 8080')
});