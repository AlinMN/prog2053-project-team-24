import {Router} from '@vaadin/router';
import 'server/views/index.ejs'
import 'server/views/login.ejs'
import 'server/views/register.ejs'


const outlet = document.getElementById('outlet');
const router = new Router(outlet);

router.setRoutes([
    {path: '/', component: 'home-page'},
    {path: '/login', component: 'login-page'}
]);