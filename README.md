# Prosjekt - PROG2053

## Installation notes: 
- Install nodeJs
- Run "npm i" in both the server folder of the project.    
- While in the root folder of the project: "docker-compose up -d" - this will take a long time the first time you do it.
- Are you getting an error after doing "docker-compose up -d", saying "[...] Filesharing has been cancelled"? 
-> Go to Docker for Windows app (or similar) -> Settings -> Resources -> File sharing -> Add all your drives (or play around with figuring out what exactly you need).
- The nodejs server might crash if the database is not initialized prior to running, it can be restarted
by restarting the docker container, or by adding a empty line in server.js

Want to reset your containers and volumes fully? 
- "docker system prune -a --volumes"

Want to get in to a container for some reason? 
- "docker-compose exec <containername> bash" 

## Group members:     
Jonas Kjærandsen

Alin Mihai Napirca
   
## Setup: 
- docker-compose up -d   

Users already in the database:
Email: user@user.user
Password: user
Email: mod@mod.mod
password: mod
Email: admin@admin.admin
Password: admin

The project runs on localhost:8080

Database runs on localhost:8082
User: admin
Password: password


## How to use
On the login page use one of the account details of one of already existing users, or create an own user using the "Register button"
On the the registration page create an user with an email address and a password, email address has to contain the "@" sign to be able to create account.

Upon arriving on the homepage the user will be shown the different users registered on the forum with their ID and email showing as a list.
The user can click on any of the shown users, including himself/herself and be redirected to the chosen users page.
At the chosen user's page the user can see all posts(if there are any), that is the title and the content of each individual post made by the chosen user.
If the user chooses to click on one of the posts he will be redirected to the post page where he can see the post title and content, but also comments made (if there are any) by users on the forum.
The user can then click on the user ID next to the comment to go to the user page of the user that made the comment.

The XYZFORUM title is a button that will return the user to the homepage.
On the righthand side of the page the user is able to see his email and a logout button, that when pressed will end the session and disconnect the users account and he will be prompted back to the login page.