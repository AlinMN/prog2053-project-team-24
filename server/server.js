"use strict";

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}
 
// Constants and variables
const crypto = require('crypto');
const express = require('express')
// const cookieParser = require('cookie-parser');
const app = express();
const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')
const methodOverride = require('method-override')
const mysql = require('mysql')
const db = require('./lib/dbconnection.js');
const PORT = 8080;
const LocalStrategy = require('passport-local').Strategy
var BetterMemoryStore = require('session-memory-store')(session);
var Store = require('express-session').Store;
var store = new BetterMemoryStore({ expires: 60 * 60 * 1000, debug: true });




// Connecting to the database from ./lib/dbconnection.js
db.connect(function (err) {
  if (err) {
    throw err;
  }
  console.log("Log: Connection with the database established.");
});

app.set('view-engine', 'ejs')
app.use(express.urlencoded({ extended: false }))
app.use(flash())
app.use(session({
  name: 'ASESSION',
  secret: '4Myu6mV4NUu7cE',
  store:  store,
  resave: true,
  saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))

// Renders the home page if the user is logged in
app.get('/', checkAuthenticated, (req, res) => {
  // Get the users to show them
  db.query('SELECT * FROM users', function(err,data) {
	  if(err) console.log(err)
		console.log(data);
		data.forEach(function(element) {
			console.log("USER:");
			console.log(element.uid + " " + element.email);
		});
		console.log("done");
		res.render('home.ejs', { name: req.user.email,testquery: data})
  })
 
})

app.get('/login', checkNotAuthenticated, (req, res) => {
 /* setCookie('session_ID', crypto.randomInt(10000), Date());
  getCookie('email'); */
  res.render('login.ejs')
})

// Login, checks if the user is already logged in
app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash: true
}))

// Renders a users page and his posts from the users id
app.get('/users/:id', checkAuthenticated, (req, res) => {
	getUser(req.params.id, function(returnValue) {
	var username = returnValue;
	if (returnValue && returnValue.length ) {
		console.log("testuser should appear here:");
		console.log(username);
		//posts = getPosts
		getPosts(req.params.id, function(returnValue) {
		var posts = returnValue;
		var comments = new Array();
	
	posts.forEach(function(post){comments.push(post);console.log("postid: ");console.log(post)});
	console.log("comments:");
	console.log(comments);
	
		res.render('users.ejs', { name: username, posts: posts, username: req.user.email} );
	});
	} else {
		console.log('invalid user');
		res.redirect('/');
	};
	});
})

// Renders a specific post by its id
app.get('/posts/:id', checkAuthenticated, (req, res) => {
  getPost(req.params.id, function(returnValue) {
	var posts = returnValue;
	if (returnValue && returnValue.length ) {
		console.log("testuser should appear here:");
		getUser(posts[0].user, function(returnValue) {
		var username = returnValue;
		console.log(username);
		var comments = new Array();
	console.log(posts[0].pid);
	getComments(posts[0].pid, function(returnValue) {
	comments = returnValue;
		res.render('post.ejs', { name: username, posts: posts, username: req.user.email, comments: comments} );
	});
	});
	} else { 
		console.log('invalid user');
		res.redirect('/');
	};
	});
});

// Returns the email of the specified user from the database
function getUser(id, callback){
	console.log("getuserbyidhere");
	console.log(id);
	db.query('SELECT * FROM users WHERE uid = ' + mysql.escape(id), function(err,data) {
	  if(err)  console.log(err)
	    // Checks if the entry exists to prevent crashes
	    if (data && data.length ) {
		console.log("post user exists");
		console.log(data[0].email);
		callback(data[0].email);
		} else {
		console.log("done retrieving user email");
		callback(data);
		}		
  })
}

// Returns the a specific post from the database
function getPost(id, callback){
	console.log("getuserbyidhere");
	console.log(id);
	db.query('SELECT * FROM posts WHERE pid = ' + mysql.escape(id), function(err,data) {
	  if(err)  console.log(err)
	    // Checks if the entry exists to prevent crashes
	    if (data && data.length ) {
		console.log("post exists");
		console.log(data);
		callback(data);
		} else {
		callback(data);
		}		
  })
}

// Returns all posts by a specified user from the database
function getPosts(id, callback){
	console.log("getuserbyidhere");
	console.log(id);
	db.query('SELECT * FROM posts WHERE user = ' + mysql.escape(id), function(err,data) {
	  if(err)  console.log(err)
		console.log(data);
		console.log("done");
		callback(data);
  })
}

// Returns the comments of a specified post from the database
function getComments(id, callback){
	console.log("getcommentbyidhere");
	console.log(id);
	db.query('SELECT * FROM comments WHERE post = ' + mysql.escape(id), function(err,data) {
	  if(err)  console.log(err)
		console.log(data);
		console.log("done");
		callback(data);
  })
}

// Checks if the user is authenticated, else redirects to the login page
function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }

  res.redirect('/login')
}

// Checks if the user is not authenticated, for login purposes
function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect('/')
  }
  next()
}

// Renders the register page if the user is not authenticated
app.get('/register', checkNotAuthenticated, (req, res) => {
  res.render('register.ejs')
})

// Adds a user to the database if the email is not already in use
app.post('/register', checkNotAuthenticated, async (req, res) => {
  try {
	// Insert the user into the database
	db.query('SELECT * FROM users WHERE email = ' + mysql.escape(req.body.email), function (err, result) {
      if (err) {
        res.status(400).send('Error in database operation.');
	    console.log("error");
	  } else {
	    // If a user with the specified email already exists
		if (result && result.length ) {
		  console.log("ROW EXISTS");
		  res.redirect('/register');
		// Else create the user and redirect them to the login page
		} else {
		  console.log("ROW DOESN'T EXIST");
		  
		  // Creating a random salt for the user using crypto
		  var salt = crypto.randomBytes(40).toString('hex').substring(0,40);
		  console.log(salt);
		  // Salting the password then hashing it
		  var saltedPassword = salt+''+req.body.password;
		  var hashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');
		  
		  let post = {email: req.body.email, password: hashedPassword, salt: salt, usertype: 'user', picture:null};
	      let sql = 'INSERT INTO users SET ?';
		  let query = db.query(sql, post, (err, result) => {
		  if(err) throw err;
		    console.log(result);
          });
          res.redirect('/login')
        }
    }
  });
	// Error redirects to register
  } catch {
    res.redirect('/register')
  }
})

// Logs the user out
app.delete('/logout', (req, res) => {
  req.logOut()
  //res.clearCookie('session_ID')
  res.redirect('/login')
})

// Passport checking of credentials
passport.use('local', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true //passback entire req to call back
} , function (req, email, password, done){
      if(!email || !password ) { return done(null, false, req.flash('message','All fields are required.')); }
      db.query("select * from users where email = ?", [email], function(err, rows){
          console.log(err); console.log(rows);
        if (err) return done(req.flash('message',err));
        if(!rows.length){ return done(null, false, req.flash('message','Invalid email or password.')); }
		var dbPassword  = rows[0].password;
		// Salting the password then hashing it
		var salt = rows[0].salt;
		var saltedPassword = salt + '' + password;
	    var hashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');
        if(!(dbPassword == hashedPassword)){
            return done(null, false, req.flash('message','Invalid email or password.'));
         }
        return done(null, rows[0]);
      });
    }
));

// Complete passport login
passport.serializeUser(function(user, done){
    done(null, user.uid);
});

// Passport logout
passport.deserializeUser(function(id, done){
    db.query("select * from users where uid = "+ id, function (err, rows){
        done(err, rows[0]);
    });
});

app.listen(PORT)