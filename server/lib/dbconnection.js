// Database credentials

var mysql = require('mysql');
var db = mysql.createConnection({
  host: "db",
  user: "admin",
  password: "password",
  database: 'prog2053-proj'
});

module.exports = db;