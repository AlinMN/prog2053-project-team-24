/* Create database */
CREATE DATABASE IF NOT EXISTS `prog2053-proj`;

/* Create tables */
CREATE TABLE `users` (
  `uid` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(128) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `salt` VARCHAR(40) NOT NULL,
  `userType` ENUM('admin', 'moderator', 'user') DEFAULT "user" NOT NULL,
  `picture` LONGBLOB DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `posts` (
`pid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`user` BIGINT(8) NOT NULL,
`title` VARCHAR(200) NOT NULL, 
`content` VARCHAR(20000) NOT NULL,
PRIMARY KEY (`pid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `comments` (
`cid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`post` BIGINT(8) NOT NULL,
`user` BIGINT(8) NOT NULL,
`comment` VARCHAR(20000),
PRIMARY KEY (`cid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`),
FOREIGN KEY(`post`) REFERENCES posts(`pid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

/* Insert data */
INSERT INTO `users` (`uid`, `email`, `password`, `salt`, `userType`) VALUES ('1','user@user.user','33934c1745fc2b3e4bd00476865d5ae4b3fcb43c31c00f6bb391948a70e67553','69bbd67c89f44350299a9a82dd54764836f1b388','user'),
('2','mod@mod.mod','aaf975b9c8ee17211dc32a905b6e353dfbfeaa452cda0d4009e5d7b48b4e73c7','a8f61bbfb33ade85c317c1539fe3d1ee10a0ba46','moderator'),
('3','admin@admin.admin','85d3d78e5d4d76225df0a401151ad8c66b9a69ab6dadd8bade09b8bfdc5772c5','f36c4edd3035cee1275976e0a38f972e0c3126b2','admin');

INSERT INTO `posts` VALUES ('1','3','Animi ut occaecati omnis iure magnam aliquam quam.','Dolorum beatae porro autem et possimus qui eum. Et facilis soluta quo distinctio. Voluptatem quam quia fugiat quaerat dolore aut. Autem autem aut minus quia optio.'),
('2','2','Nobis laboriosam totam labore aut possimus pariatur recusandae.','Accusantium unde dignissimos quia ab quas corporis. Impedit possimus rerum ut ratione qui et a. Illum cum fugit atque.'),
('3','1','Optio consequuntur sint tempore molestiae aut esse.','Modi quia eius natus consectetur ab ut. Animi facilis quam placeat. Illo nulla autem ut qui voluptate aut.');

INSERT INTO `comments` VALUES ('1','1','2','Possimus nesciunt dicta neque. Sint voluptatem sequi aliquid voluptas beatae. Sed incidunt ad voluptas ut facere. Molestiae id qui commodi molestiae mollitia dolorum voluptatem eos.'),
('2','2','1','Modi ut et soluta deserunt. Saepe qui nesciunt illum quis in est. Quia dignissimos tenetur nam accusantium accusantium vitae libero.'),
('3','3','3','Repudiandae officia tempora dolore illum. Quis perspiciatis fugiat maiores inventore. Ut sit et velit debitis doloribus error. Culpa reiciendis soluta dolores libero fugit explicabo.');
